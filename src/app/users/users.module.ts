import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListadoComponent} from "./pages/listado/listado.component";
import {UserComponent} from "./pages/user/user.component";
import {PrincipalComponent} from './pages/principal/principal.component';
import {MaterialModule} from "../material/material.module";
import {UsersRoutingModule} from "./users-routing.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ListadoComponent,
    UserComponent,
    PrincipalComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class UsersModule {
}
