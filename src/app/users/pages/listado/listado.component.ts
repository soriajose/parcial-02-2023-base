import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UserListResponseDTO} from "../../interfaces/user-list-response-dto";

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  userList: UserListResponseDTO[] = [];

  constructor(private router: Router,
              private userService: UsersService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.initList();
  }

  initList() {
    this.userService.getListUser().subscribe({
      next: value => {
        this.userList = value;
      },
      error: err => {
        this.showSnackBar('Error al buscar los usuarios: ' + err.errors.message);
      }
    });
  }

  addData() {
    this.router.navigate(['/users/user']);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje);
  }
}
