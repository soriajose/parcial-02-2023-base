import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";
import {UserRequestDto} from "../../interfaces/user-request-dto";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    name: [null, [Validators.required, Validators.min(3)]],
    job: [null, [Validators.required, Validators.min(3)]]
  });
  userRequestDTO: UserRequestDto = {
    name: '',
    job: ''
  };

  constructor(private fb: FormBuilder,
              private matSnackBar: MatSnackBar,
              private userService: UsersService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar() {
    if (this.miFormulario.valid) {

      this.userRequestDTO.name = this.miFormulario.value.name;
      this.userRequestDTO.job = this.miFormulario.value.job;

      this.userService.guardarUser(this.userRequestDTO).subscribe({
        next: value => {
          this.showSnackBar('El usuario fue creado con exito');
        },
        error: err => {
          this.showSnackBar('Error al crear el usuario: ' + err.errors.message);
        }
      });
    } else {
      this.showSnackBar("Datos incompletos");
    }
  }

  cancelar() {
    this.router.navigate(['/users/listado']);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje);
  }
}
