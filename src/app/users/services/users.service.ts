import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserRequestDto} from "../interfaces/user-request-dto";
import {Observable} from "rxjs";
import {UserResponseDto} from "../interfaces/user-response-dto";
import {UserListResponseDTO} from "../interfaces/user-list-response-dto";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) {
  }

  guardarUser(user: UserRequestDto): Observable<UserResponseDto> {
    return this.httpClient.post<UserRequestDto>('https://reqres.in/api/users', user);
  }

  getListUser(): Observable<UserListResponseDTO[]> {
    return this.httpClient.get<UserListResponseDTO[]>('https://reqres.in/api/users?page=1');
  }

}
